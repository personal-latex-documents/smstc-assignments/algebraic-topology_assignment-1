<head>
	<style>
	h2{
		border-bottom: black solid 1px;
	}
	h3{
		border-bottom: black dashed 1px;
	}
	h4{
		border-bottom: black dotted 1px;
	}
	</style>
</head>
## Question 1

###  Part 1(i)

\begin{align}
H &= \cup_{n \geq 1} H_n \subseteq \mathbb{R}^2\qquad
\text{where} \:
H_n = \left\{
	\left(\frac1n + \frac1n cos(2\pi\theta), sin(2\pi\theta) \right)
	\colon 
	\theta \in [0,2\pi) 
\right\}
\\
X &= \vee_{n \geq 1}S^1 \subseteq \mathbb{C} \qquad
\text{where the } S^1 \text{ are attached at } (-1,0)
\\
\end{align}

#### There is a continuous bijection $f \colon X \rightarrow H$.

$f_n \colon S^1 \rightarrow H_n$ given by 
$f_n(e^{i\theta}) = \left(\frac1n + \frac1n cos(2\pi\theta), sin(2\pi\theta) \right)$
is a homeomorphism for each $n$. Hence induces a continuous surjection 
$\sqcup_n S^1 \rightarrow H = \cup_n H_n$
given by $f_n$ on the $n$th copy of $S^1$.
The $H_n$ only intersect at $(0,0)$, whose preimage is $-1$ in each copy of $S^1$ in $\sqcup_n S^1$.
Therefore, this further induces a continuous bijection 
$f \colon X = \vee_n S^1 \rightarrow H$.


#### $H$ and $X$ are not homeomorphic

$X$ is locally contractible, but $H$ is not at $(0,0)$ (any neighbourhood contains one of the $H_n$).

Alternatively (click for details):
<details>
<summary>$X$ is not compact.</summary> To see this consider an open cover consisting of each copy of $S^1$ with $-1$, the attaching point, removed, along with the union of all the $S^1$ with $1$ removed from each of them.
</details>
<details>
<summary>But $H$ is compact.</summary> It is bounded in $\mathbb{R}^2$ and I also claim it's closed. To show this, let $x\in \mathbb{R}^2 \setminus H$. $H$ consists of concentric circles so $x$ is either outside of the biggest circle, or between 2 of them. If it's outside of the biggest then we can clearly take a suitably small ball around it not intersecting $H$.
Otherwise, let $A$ be the smallest circle containing $x$ and $B$ be the smallest not containing $x$. Then if we had a ball around $x$ that didn't intersect $A \cup B$ then it wouldn't intersect $H$ either. But $A \cup B$ is compact so $dist(x,A\cup B)$ is well defined and strictly positive, so we can take this as the radius for the ball around $x$ that we need. Having shown $H$ is both closed and bounded, it is therefore compact by Heine-Borel.
</details>

###  Part 1(ii)

Denote the $n$th copy of $S^1$ in $X$ as $S^1_n$, and if $x\in S^1$, denote $x_n$ the corresponding element in $S^1_n$.
Let $x_0$ be the attaching point of the wedge sum ($-1$ in each $S^1_n$).

Let $A_n = X \setminus \left(\cup_{i \neq n} 1_i\right)$ which is open in $X$, path connected, contains the atteching point $x_0$ and homotopy retracts to $S^1_n$. The $A_n$ for $n=1,2...$ also form an open cover of $X$.

$A_n \cap A_m = X \setminus \left(\cup_{i} 1_i\right)\:$ when $m \neq n$
, which is path connected and contractible.

$A_n \cap A_m \cap A_l = X \setminus \left(\cup_{i} 1_i\right)\:$ when $n,m,l$ are not all equal
, which is again, path connected and contractible.

The latter two spaces have trivial fundamental group so the $N$ in the statement of the van Kampen Theorem is also trivial.
This gives 
$$\pi_1(X, x_0) = \ast_{n \in \mathbb{N}} \pi_1(A_n,x_0) = \ast_{n \in \mathbb{N}} \mathbb{Z}$$
the free group on $\mathbb{N}$.

###  Part 1(iii)

#### There exists a surjective homomorphism $\pi_1(H,(0,0)) \rightarrow \Pi_{n \geq 1} \mathbb{Z}$

Define the following projections:
$$ p_n \colon H \rightarrow \frac{H}{\cup_{m \neq n}H_m} \cong S^1$$
Let
\begin{align}
\phi \colon \pi_1(H,(0,0)) &\rightarrow \Pi_{n \geq 1} \mathbb{Z} \\
l &\mapsto \left(
			(p_n)_*l
\right)_{n \geq 1}
\end{align}
which is a well defined homomorphism (since each $(p_n)_*$ is).

Now I claim that it is surjective, to show this, take any $(a_n)_{n \geq 1} \in \Pi_{n \geq 1} \mathbb{Z}$.\
Define a path $p \colon I \rightarrow H$ as follows:\
Wrap $a_n$ times around $H_n$ (starting and ending at $(0,0)$) over $[1 - 2^{1-n}, 1 - 2^{-n}]$.
These piece together to a continuous function $[0,1) \rightarrow H$ but since the $H_n$ shrink down to $(0,0)$ then this extends to a continuous function $I \rightarrow H$ by mapping $1$ to $(0,0)$.  
Finally, it is clear that $\phi([p]) = (a_n)_{n \geq 1}$.  
$\therefore \phi$ is a surjective homomorphism $\pi_1(H,(0,0)) \rightarrow \Pi_{n \geq 1} \mathbb{Z}$.

#### $\pi_1(H,(0,0)) \not\cong \pi_1(X,x_0)$

$\phi$ surjects $\pi_1(H,(0,0))$ onto an uncountable set, and hence $\pi_1(H,(0,0))$ itself is not countable. But $\pi_1(X,x_0)$, the free group on $\mathbb{N}$, **is** countable. So these two groups cannot biject, let alone be isomorphic.  
$\therefore \pi_1(H,(0,0)) \not\cong \pi_1(X,x_0)$

###  Part 1(iv)

Recall from [Part 1(i)] that $H$ is compact.  
Now suppose, seeking a contradiction, that $H$ is homotopy equivalent to a CW-complex $C$, then there are maps $f,g$ such that:
$$H \mathrel{\mathop{\rightleftarrows}^f_g} C \qquad gf \simeq id_H \qquad fg \simeq id_C$$
$f(H)$ is compact, so is contained in some finite subcomplex $D$ of $C$ (as is given in the question).  
So restrict the codomain of $f$ to $D$ to give the following diagram:


\begin{matrix}
   &                & H \:\:\:         \\
   & \swarrow{f}    & \uparrow{g} \\
 D & \hookrightarrow & C\:\:\: 
\end{matrix}

Where $\hookrightarrow$ is the inclusion $i_D \colon D \hookrightarrow C$

Applying $\pi_1$ we get:
\begin{matrix}
          &                & \pi_1(H)          \\
          & \swarrow{f_*}  & \uparrow{g_*}     \\
 \pi_1(D) & \longrightarrow     & \pi_1(C) 
\end{matrix}

Since $i_D f$ and $g$ give a homotopy equivalence, their images under $\pi_1$ are isomorphisms.  
In particular, $(i_D)_* f_*$ is injective, and so, $f_* \colon \pi_1(H) \rightarrow \pi_1(D)$ is too.
$D$ is a finite CW-complex hence $\pi_1(D)$ is countable, but $\pi_1(H)$ is not countable, giving a contradiction. 
Uncountable sets cannot inject into countable ones $\unicode{x21af}$.  
$\therefore$ H is not homotopy equivalent to any CW-complex.




## Question 2

###  Part 2(i)

a) The Torus $T^2$
b) The Klein bottle
c) The Möbius strip

This was done by thinking about these spaces as squares with side identifications.

### Part 2(ii)

#### Part 2(ii)(a)

Note that in this question I will change the order of "$U\times X$" to "$X \times U$" to be more consistent with the definition of $T_f$ given.

##### There is a canonical map $p \colon T_f \rightarrow S^1$ with the following property: for every point $z\in S^1$ there is an open neighbourhood $U$ of $z$ and a homeomorphism $\Phi \colon p^{-1}(U) \rightarrow X \times U$ such that $pr_2 \circ \Phi = p|_{p^{-1}(U)}$

Define the map:
\begin{align}
p &\colon& T_f&\rightarrow\frac{[0,1]}{0 \sim 1} &\cong &S^1 \\
&& [(x,t)]&\mapsto[t]&\mapsto &e^{2\pi it}
\end{align}
Let $U_1 = S^1 \setminus \{1\} \cong (0,1)$
and $U_2 = S^1 \setminus \{-1\} \cong \frac{[0,\frac12)\cup(\frac12,1]}{0 \sim 1}$,
giving an open cover $\{U_1,U_2\}$ of $S^1$.

Define 
\begin{align}
\Phi_1 \colon p^{-1}(U_1) &\rightarrow& X \times (0,1)     &\cong X \times U_1 \\
              [(x,t)]     &\mapsto& (x,t)                  &\mapsto (x,e^{2\pi it})\\
\Phi_2 \colon p^{-1}(U_2) &\rightarrow& X \times \frac{[0,\frac12)\cup(\frac12,1]}{0 \sim 1} &\cong X \times U_2 \\
              [(x,t)]     &\mapsto& [(x,t)]                         &\mapsto (x,e^{2\pi it})          &\mathrm{if}\: t>\frac12\\
              [(x,t)]     &\mapsto& [(f(x),t)]                      &\mapsto (f(x),e^{2\pi it})       &\mathrm{if}\: t<\frac12
\end{align}
The latter is well defined because $(x,0) \sim (f(x),1)$ in $T_f$, and is an isomorphism because $f$ is.
\begin{align}
pr_2 \circ \Phi_{i} \colon p^{-1}(U_i) &\rightarrow U_i \\
                           [(x,t)]     &\mapsto     e^{2\pi it}
\end{align}
for both $i=1$ and $2$, which coincides with $p$.  
$\therefore \{U_1, U_2\}$ is a trivializing open cover in the fibre bundle $p \colon T_f \rightarrow S^1$.

#### Part 2(ii)(b)

Define $h_1 = \frac12(1 - Re(\_)) , h_2 = \frac12(1 + Re(\_)): S^1 \rightarrow I$, so that $U_i = h_i^{-1}((0,1])$.
So $\{U_1, U_2\}$ is a numerable trivializing open cover of the fibre bundle $p \colon T_f \rightarrow S^1$.  
Therefore, by **Theorem 1.90**, $p \colon T_f \rightarrow S^1$ is a fibration.  
$\therefore G$ in the diagram exists for any $H$ and $G_0$.

#### Part 2(ii)(c)

The inclusion
\begin{align}
i \colon X &\hookrightarrow T_f       \\
           X &\mapsto     [(x,0)]
\end{align}
induces a map 
$i_* \colon \pi_1(X,x_0) \rightarrow \pi_1(T_f,[(x_0,0)])$
  
And I claim that it fits in a short exact sequence:
$$ 1 
\rightarrow \pi_1(X,x_0) 
\mathrel{\mathop{\rightarrow}^{i_*}}
\pi_1(T_f,[(x_0,0)]) 
\mathrel{\mathop{\rightarrow}^{p_*}}
\pi_1(S^1,1) 
\rightarrow 1
$$

##### $p_*$ is surjective:
The diagram in [Part 2(ii)(b)] for the case $n=0$ gives us that any map $I \rightarrow S^1$ can be factored through $p\colon T_f \rightarrow S^1$.
In particular, any loop in $S^1$ can be lifted through $p$.  
$\therefore p_*$ is surjective.

##### Exactess at $\pi_1(T_f,[(x_0,0)])$:
Take any $[l] \in ker(p_*)$. Consider the diagram from [Part 2(ii)(b)] for the case $n=1$, take $G_0 = l$ and $H$ to be the
homotopy taking $l$ to the constant map $c_{1}$ (relative endpoints).  
Now consider the corresponding $G$
![G and H](2iic1.png)
Pull out the left corners to get $G'$:
![G'](2iic2.png)
Which is a homotopy relative endpoints between $l$ and a path in $i(X)$., that is, $[l] \in Im(i_*)$.  
So $ker(p_*) \subseteq Im(i_*)$, and the other inclusion is immediate by the fact that $p \circ i$ is a constant map so $p_* \circ i_*$ is trivial.  
$\therefore ker(p_*) = Im(i_*)$  

##### $i_*$ is injective:

Take any $[l] \in ker(i_*)$, then there is a homotopy relative endpoints $G \colon i\circ l \simeq c_{[(x_0,0)]}$
$p \circ G_0$ then maps the boundary of $I \times I$ to $1 \in S^1$. Since $\pi_2(S^2) = 1$, then there is a homotopy
relative boundary $H \colon p \circ G_0 \simeq c_1$. $H$ maps the whole of the boundary to $1\in S^1$ apart from possibly the
face corresponding to $p \circ G_0$ which we shall call the "top face". Take this $G_0$ and $H$ in the diagram in [Part 2(ii)(b)]
with $n=2$. Then the lift $G \colon I^3 \rightarrow T_f$ restricted to the top face is $G_0$ and the rest of the boundary is mapped
into $p^{-1}(1) = i(X)$.  
![G2](2iic3.png)
Looking at $G$ restricted to the boundary with the top face removed,  
![G2'](2iic4.png)
we can construct a homotopy (rel endpoints) 
![G2''](2iic5.png)
$i\circ l \simeq c_{[(x_0,0)]}$ which stays in $i(X)$.  
Using the obvious isomorphism $X \cong i(X)$ given by $i$, we get a homotopy rel. endpoints $l \simeq c_{x_0}$.  
$\therefore ker(i_*) = 1$

Therefore the sequence
$$ 1 
\rightarrow \pi_1(X,x_0) 
\mathrel{\mathop{\rightarrow}^{i_*}}
\pi_1(T_f,[(x_0,0)]) 
\mathrel{\mathop{\rightarrow}^{p_*}}
\pi_1(S^1,1) 
\rightarrow 1
$$
is exact.

#### Part 2(ii)(d)
\begin{align}

\gamma \colon \pi_1(S^1,1) &\rightarrow \pi_1(T_f,x_0) \\
              [l]          &\mapsto     [(c_{x_0},l)]
\end{align}
is a section of $p_*$, so $\pi_1(T_f,[(x_0,0)]) \cong \pi_1(X,x_0) \ltimes_{\phi} \pi_1(S^1,1)$
where
\begin{align}
\phi \colon \mathbb{Z} \cong \pi_1(s^1,1) &\rightarrow Aut(\pi_1(X,x_0)) \\
            n                             &\mapsto     \phi_n            \\
			\phi_n([l]) &= i_*^{-1}(\gamma(n)[l]\gamma(n)^{-1}) \\
\end{align}
$\gamma(1)$ is represented by the loop $p(t)=[(x_0,t)]$. Homotoping $plp^{-1}$ into $i(X)$ "shifting along $I$" turns the loop $l$ into $f\circ l$ because of the identification
$(x,0) \sim (f(x),1)$ in $T_f$. And more generally, $p^nlp^{-n}$ homotopes to $f^n \circ l$.  
So $\phi_n([l]) = [f^n\circ l]$, i.e. $\phi_n = f_*^n$.
And $\mathbb{Z} \cong \pi_1(s^1,1)$, so we can write $\pi_1(T_f,[(x_0,0)]) \cong \pi_1(X,x_0) \ltimes_{f_*} \mathbb{Z}$.

###  Part 2(iii)

Let's apply [Part 2(ii)(d)] to the mapping torus of 
\begin{align}
f \colon S^1 \times S^1 &\rightarrow S^1 \times S^1 \\
             (a,b)      &\mapsto    (b,a)
\end{align}
((1,1) is a fixed point so it can indeed be applied).
$\pi_1(S^1 \times S^1) \cong a\mathbb{Z}\times b\mathbb{Z}$, the free abelian group on 2 elements $a,b$ which correspond to the generators of the two $\pi_1(S^1)$.  
So $\pi_1(T_f) \cong (a\mathbb{Z}\times b\mathbb{Z}) \ltimes_f \mathbb{Z}$ where $f \colon a\mathbb{Z}\times b\mathbb{Z} \rightarrow a\mathbb{Z}\times b\mathbb{Z}$ given by $f(a)=b, f(b)=a$.

## Question 3

###  Part 3(i)

Let
\begin{align}
r \colon M_f &\rightarrow Y \\
     Y \ni y &\mapsto y     \\
    X \times I \ni (x,s) &\mapsto f(x) \\
\end{align}
I actually defined a map from $X \times I \sqcup Y$ but it agrees between the points being glued together so induces a map from $M_f$. 

$r \circ i_Y = id_Y$ where $i_Y \colon X \rightarrow M_f$ is the inclusion of $Y$ into $M_f$.  
And $i_Y \circ r \simeq id_{M_f}$ via the the homotopy given by:
\begin{align}
h_t \colon M_ &\rightarrow M_f \qquad t \in I \\
           y   &\mapsto     y   \\
		 (x,s) &\mapsto (x,ts+(1-t))\\
\end{align}
so that $h_1 = id_{M_f}$ and $h_0 = i_Y \circ r$.  
Therefore, $r$ is a retraction.  
Finally, consider the inclusion $i \colon X \rightarrow M_f$ of $X$ at $0$.  
$r(i(x)) = r((x,0)) = f(x)$.  

$\therefore r$ is a deformation retraction such that $f = r \circ i$.

###  Part 3(ii)

Suppose $f_* \colon \pi_n(X,x_0) \rightarrow \pi_n(Y,f(x_0))$ is an isomorphism for every $n \in \mathbb{N}$.  

##### $(M_f, i(X))$ is an NDR pair

Let 
\begin{align}
u \colon M_f &\rightarrow I \\
(x,s) &\mapsto 2s \qquad &s \leq \frac12 \\
(x,s) &\mapsto 1 \qquad &s \geq \frac12 \\
 y    &\mapsto 1 \\
\end{align}
so that $i(X) = u^{-1}(0)$ and $u^{-1}([0,1)) = X \times [0,\frac12) \subset M_f$  

And define
\begin{align}
h_t \colon M_f &\rightarrow M_f \\
   (x,s) &\mapsto (x,(1-t)s) \qquad &s \leq \frac12 \\
   (x,s) &\mapsto (x,\frac12(1-t) + (1+t)(s-\frac12))) \qquad &s \geq \frac12 \\
   y     &\mapsto y
\end{align}
<details>
<summary>which is well defined (click for details)</summary>
Sanity checks:  
For $s=0$, $(1-t)s = 0$  
For $s=\frac12$, $(1-t)s = \frac12(1-t)$ and $\frac12(1-t) + (1+t)(s-\frac12) = \frac12(1-t) + (1+t)(0) = \frac12(1-t)$  
For $s=1$, $\frac12(1-t) + (1+t)(s-\frac12) = \frac12(1-t) + \frac12(1+t) = 1$  
So what I defined was consistent
</details>

<details>
<summary>This shows that $(M_f,i(X))$ is an NDR pair.</summary>
$h_0 = id_{M_f}$ and $h_t|_{X\times\{0\}} = id_{X\times\{0\}}$, but also notice that $h_1((x,s)) = (x,0) \in X \times \{0\} = i(X)$ when $s \leq \frac12$.  
That means $h_1(u^{-1}([0,1)) \subseteq i(X)$.
</details>

$\therefore (M_f, i(X))$ is an NDR pair

##### Long exact sequence in homotopy groups

We can now apply theorem 1.84, there is a long exact sequence:
\begin{align}
...\rightarrow
\pi_n(X,x_0) 
&\mathrel{\mathop{\rightarrow}^{i_*}}
\pi_n(M_f,x_0) 
\mathrel{\mathop{\rightarrow}^{j_*}}
\pi_n(M_f,X,x_0) 
\mathrel{\mathop{\rightarrow}^{d}} \\
\pi_{n-1}(X,x_0) 
&\mathrel{\mathop{\rightarrow}^{i_*}}
\pi_{n-1}(M_f,x_0) 
\rightarrow ...
\end{align}
$f_* = r_* \circ i_*$ (recall we showed $f=ri$) but $f_*$ is an isomporphism (for each $n$) by assumption and $r_*$ is also an isomorphism because it is a retraction.  
Therefore $i_*$ is also an isomporphism.

<details>
<summary>It follows from exactness of the sequence that $\pi_n(M_f,X,x_0) = 1$ for each $n$</summary>
$i_*$ is surjective so $ker(j_*)$ is the whole domain, so in particular, $Im(j_*)$ is just the identity.  
$i_*$ is also injective so $Im(d)$ is just the identity, and so $ker(d)$ is the whole of $\pi_n(M_f,X,x_0)$.  
But $ker(d) = Im(j_*)$ by exactness so $\pi_n(M_f,X,x_0) = 1$
</details>
















